var myShortcut = {
    KeyboardEventHandler: class {
        constructor () {
            this._initialize();
        }
    
        _initialize () {
            this.groups = {};
            this.map = {
                keypress: {},
                keydown: {},
                keyup: {},
            };
        }
    
        _update () {
            this.map = {
                keypress: {},
                keydown: {},
                keyup: {},
            };
            for (var i in this.groups) {
                if (this.groups[i] == null) continue;
                var kObjs = this.groups[i].children;
                for (var j in kObjs) {
                    var kObj = kObjs[j];
                    var evt = kObj.evt;
                    var key = kObj.key;
    
                    if (this.map[evt] == null)
                        continue;
                    if (key == null)
                        continue;
                    if (this.map[evt][key] == null)
                        this.map[evt][key] = {};
                    this.map[evt][key][i] = kObj.callback;
                }
            }
    
            console.log(this.map)
            Mousetrap.reset();
            for (var evt in this.map) {
                for (var key in this.map[evt]) {
                    var tmparr = [];
                    for (var uid in this.map[evt][key]) {
                        if (this.groups[uid] == null)
                            continue;
                        if (this.groups[uid].active !== true)
                            continue;
                        var callback = this.map[evt][key][uid];
                        if (callback != null)
                            tmparr.push(callback);
                    }
                    var combine = this._get_callback(tmparr);
                    console.log(combine)
                    Mousetrap.bind(key, combine, evt);
                }
            }
        }
    
        _add (uid, kObj) {
            if (kObj instanceof myShortcut.KeyboardEvent) {
                if (this.groups[uid] == null)
                    this.groups[uid] = {
                        children: [],
                        active: true
                    };
                this.groups[uid].children[kObj.uid] = kObj;
            } else if (kObj instanceof myShortcut.KeyboardEventGroup) {
                if (this.groups[uid] != null)
                    console.warn('object with uid='+uid+' already exists. replace');
                for (var i in kObj.children) {
                    this._add(uid, kObj.children[i]);
                }
                this.groups[uid].name = kObj.name;
            }
        }
    
        _get_callback (func_arr) {
            // Input: func_arr Array of callback function
            return function (arr) {
                for (var i=0;i<arr.length;i++) {
                    console.log(arr[i])
                    arr[i]();
                }
            }.bind(null, func_arr);
        }

        reset () {
            Mousetrap.reset();
            this._initialize();
        }
    
        add (kObj) {
            if (kObj.uid == null)
                throw "uid of myShortcut.KeyboardEventGroup object cannot be null";
            var uid = kObj.uid;
            this._add(kObj.uid, kObj);
            this._update();
        }
    
        on (uid) {
            if (this.groups[uid] == null) {
                console.warn('object with uid='+uid+' does not exists.');
                return;
            }
            this.groups[uid].active = true;
            this._update();
        }
    
        off (uid) {
            if (this.groups[uid] == null) {
                console.warn('object with uid='+uid+' does not exists.');
                return;
            }
            this.groups[uid].active = false;
            this._update();
        }
    
        remove (uid) {
            if (this.groups[uid] == null) {
                console.warn('object with uid='+uid+' does not exists.');
                return;
            }
            this.groups[uid] = null;
            this._update();
        }
    
        getList () {
            var list = [];
            for (var uid in this.groups) {
                if (this.groups[uid] == null)
                    continue;
                var tmp = {
                    uid: uid,
                    name: this.groups[uid].name,
                    items: []
                };
                var kObjs = this.groups[uid].children;
                for (var j in kObjs) {
                    var kObj = kObjs[j];
                    tmp.items.push({
                        uid: kObj.uid,
                        name: kObj.name,
                        key: kObj.key,
                        evt: kObj.evt,
                        editable: kObj.editable,
                    });
                }
                list.push(tmp);
            }
            return list;
        }
    
        getActiveGroupIdArray () {
            var uid_arr = [];
            for (var uid in this.groups) {
                if (this.groups[uid] == null)
                    continue;
                uid_arr.push(uid);
            }
            return uid_arr;
        }

        deactivateAll() {
            for (var uid in this.groups)
                this.off(uid);
        }

        setEventKeycode (setting) {
            // setting: {
            //     <group uid>: {
            //         <event uid>: {
            //             evt: <event: keypress/keydown/keyup>,
            //             key: <key>
            //         }
            //     }
            // }
            if (typeof setting !== 'object')
                throw "Input argument 'setting' should be an Object";
            for (var uid in setting) {
                var grp = this.groups[uid];
                if (grp == null) {
                    console.warn("group of uid '"+uid+"' not found. skip");
                    continue;
                }
                for (var subuid in setting[uid]) {
                    if (grp.children[subuid] == null) {
                        console.warn("item of uid '"+subuid+"' in group of uid '"+uid+"' not found. skip");
                        continue;
                    }
                    if (!grp.children[subuid].editable) {
                        console.warn("item of uid '"+subuid+"' in group of uid '"+uid+"' is not editable. skip");
                        continue;
                    }
                    grp.children[subuid].evt = setting[uid][subuid].evt;
                    grp.children[subuid].key = setting[uid][subuid].key;
                }
            }
        }
    },
    
    KeyboardEvent: class {
        constructor (uid, setting) {
            // setting: {
            //     evt: <event: keypress/keydown/keyup>,
            //     key: <key>,
            //     callback: <callback function>,
            //     name: <function name>,
            //     onChangeEvtKey: <callback function when evt or key change>
            // }
            this.uid = uid;
            var attrs = {
                key: {typ:'string', def:null},
                evt: {typ:'string', def:null},
                callback: {typ:'function', def:null},
                name: {typ:'string', def:null},
                editable: {typ:'boolean', def:true},
                onChangeKeycode: {typ:'function', def:null}
            };
            for (var i in attrs) {
                var val = attrs[i];
                if (typeof setting[i] == val.typ)
                    this[i] = setting[i];
                else
                    this[i] = attrs[i].def;
            }
        }
    },
    
    KeyboardEventGroup: class {
        constructor (uid, name, kObj_list=[]) {
            this.uid = uid;
            this.name = name;
            this.children = [];
            if (kObj_list != null) {
                for (var i in kObj_list) {
                    this.children.push(kObj_list[i]);
                }
            }
        }
    }
}
