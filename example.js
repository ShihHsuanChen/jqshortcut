$(document).ready( function () {
    $('body').on('click', '.switch.unclick', function (e) {
        var tar = $(e.currentTarget);
        tar.addClass('click').removeClass('unclick');
    });
    $('body').on('click', '.switch.click', function (e) {
        var tar = $(e.currentTarget);
        tar.addClass('unclick').removeClass('click');
    });
    $('body').on('mousedown', '#c1', onC1down);
    $('body').on('mouseup', '#c1', onC1up);
    $('body').on('click', '#c2', onC2);
    $('body').on('click', '#add', add);
    $('body').on('click', '#reset', reset);
});

function onC1down () {
    $('#grp1 .line_unit[uid=item1] > span._content').css('background-color', '#FF0000');
}

function onC1up () {
    $('#grp1 .line_unit[uid=item1] > span._content').css('background-color', '#00FF00');
}

function onC2 () {
    var jObj = $('#grp1 .line_unit[uid=item2] > span._content');
    if (jObj.attr('color')=='green') {
        jObj.css('background-color', '#0000FF');
        jObj.attr('color', 'blue');
    } else {
        jObj.css('background-color', '#00FF00');
        jObj.attr('color', 'green');
    }
}

function add () {
    var num = parseInt($('#grp2 .line_unit > span._content').text());
    $('#grp2 .line_unit > span._content').text(num+1);
}

function reset () {
    $('#grp2 .line_unit > span._content').text(0);
}

$(document).ready( function () {
    var keh = new myShortcut.KeyboardEventHandler();
    var c1down = new myShortcut.KeyboardEvent('c1down', {
        name: 'Color1 Change',
        callback: onC1down,
        onChangeKeycode: function () {
        },
        evt: 'keydown',
        key: 'c'
    });
    var c1up = new myShortcut.KeyboardEvent('c1up', {
        name: 'Color1 Change',
        callback: onC1up,
        onChangeKeycode: function () {
        },
        evt: 'keyup',
        key: 'c'
    });
    var c2 = new myShortcut.KeyboardEvent('c2', {
        name: 'Color2 Change',
        callback: onC2,
        onChangeKeycode: function () {
        },
        evt: 'keyup',
        key: 'x'
    });
    var a = new myShortcut.KeyboardEvent('a', {
        name: 'Number+1',
        callback: add,
        onChangeKeycode: function () {
        },
        evt: 'keyup',
        keycode: 'shift'
    });
    var r = new myShortcut.KeyboardEvent('r', {
        name: 'Reset Number',
        callback: reset,
        onChangeKeycode: function () {
        },
        evt: 'keyup',
        keycode: 'alt+r'
    });
    
    var kg1 = new myShortcut.KeyboardEventGroup('grp1', 'group1', [c1down, c1up, c2]);
    var kg2 = new myShortcut.KeyboardEventGroup('grp2', 'group2', [a, r]);
    keh.add(kg1.uid, kg1);
    keh.add(kg2.uid, kg2);

    var sclist = keh.getList();
    console.log(sclist)

    $('#setting > ._content').append(
        // table
    );

});
